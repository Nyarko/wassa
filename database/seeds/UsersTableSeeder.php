<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Nyarko Isaac Kwadwo',
                'email' => 'nyarkoisaackwadwo@gmail.com',
                'password' => Hash::make('@Nyarko2018'),
                'contact' => '054-4474-706',
                'position' => 'Manager',
                'type' => 'admin',
                'bio' => 'The humble with all that he want intact',
                'photo' => 'avatar.png',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Nyarkoh Isaac',
                'email' => 'nyarkorep@gmail.com',
                'password' => Hash::make('@Nyarko2018'),
                'contact' => '054-9899-706',
                'position' => 'Director',
                'type' => 'admin',
                'bio' => 'The humble with all that he want intact',
                'photo' => 'avatar.png',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Rebecca Agyemang',
                'email' => 'beckyrep@gmail.com',
                'password' => Hash::make('@Nyarko2018'),
                'contact' => '054-0056-706',
                'position' => 'Manager',
                'type' => 'admin',
                'bio' => 'The stupid beautiful girl',
                'photo' => 'avatar.png',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Portia Kwekyi',
                'email' => 'portia@gmail.com',
                'password' => Hash::make('@Nyarko2018'),
                'contact' => '050-0056-706',
                'position' => 'Manager',
                'type' => 'admin',
                'bio' => 'The stupid beautiful girl',
                'photo' => 'avatar.png',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
            ]);
    }
}
