<?php

use Faker\Generator as Faker;

$factory->define(\App\User::class, function (Faker $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'contact' => $faker->phoneNumber,
        'password' => $password ?: $password = bcrypt('@admin'),
        'position' => $faker->word,
        'type' => $faker->randomElements(['Admin', 'User', 'SuperAdmin', 'Manager']),
        'bio' => $faker->sentence,
        'photo' => 'avatar.png',
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Message::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'contact' => $faker->phoneNumber,
        'body' => $faker->sentence,
    ];
});

$factory->define(\App\Project::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'body' => $faker->sentence,
        'photo' => 'avatar.png',
        'status' => $faker->randomElement(['Initiated', 'Ongoing', 'Finished', 'Restarted']),
        'user_id' => $faker->numberBetween(1,4)
    ];
});

$factory->define(App\Carousel::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'description' => $faker->sentence,
        'photo' => 'avatar.png',
    ];
});

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'comment' => $faker->sentence,
        'project_id' => $faker->numberBetween(1,20),
    ];
});

$factory->define(App\Gallery::class, function (Faker $faker) {
    return [
        'image' => 'avatar.png',
        'project_id' => $faker->numberBetween(1,20)
    ];
});

