<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carousel;
use App\Message;
use App\User;
use App\Project;
use App\Gallery;
use App\Comment;
use DB;
use Auth;
use Validator;

class PagesController extends Controller
{
    public function index(){
        $carousels = Carousel::latest()->paginate(10);
        $projects = Project::with(['user','comments'])->withCount('comments')->latest()->paginate(6);
        $galleries = Gallery::with('project')->latest()->paginate(12);
        return view('pages.index', compact('carousels','projects','galleries'));
     }

    public function Users(){
        $users = User::latest()->paginate(6);
        return view('pages.contact', compact('users'));
    }

    public function Gallery(){
        $galleries = Gallery::with('project')->latest()->paginate(18);
        return view('pages.gallery', compact('galleries'));
    }

    public function ProjectAll(){
        $projects = Project::with(['user','comments'])->withCount('comments')->latest()->paginate(12);
        return view('pages.projects', compact('projects'));
    }

    public function sendMessage(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
            'email'=>'required|email',
            'contact'=>'required',
            'body' => 'required:string',
        ]);

        if ($validator->fails()){
            return redirect()->back()->with('error','Check and fill all required field');
        }

        $message = new Message();
        $message->name = $request['name'];
        $message->email = $request['email'];
        $message->contact = $request['contact'];
        $message->body = $request['body'];

        if ($message->save()){
            return redirect()->back()->with('message','Your Message Send Successfully');
        } else {
            return redirect()->back()->with('error','Failed to send Message');
        }
    }

    public function viewProject($id){
      $project = Project::with('user')->where('id',$id)->where('deleted_at',null)->first();
      $galleries = Gallery::where('project_id',$id)->where('deleted_at',null)->get();
      $projects = Project::with('user')->latest()->paginate(6);
      $comments = Comment::where('project_id',$id)->where('deleted_at',null)->orderBy('created_at','desc')->get();
      return view('pages.viewproject', compact('project','galleries','projects','comments'));
    }

    public function sendComment(Request $request){
        $validator = Validator::make($request->all(), [
            'project_id'=>'required',
            'comment'=>'required|string',
        ]);

        if ($validator->fails()){
            return redirect()->back()->with('error','Check and fill all required field');
        }

        $comment = new Comment();
        $comment->project_id = $request['project_id'];
        $comment->comment = $request['comment'];
        $comment->save();

        return redirect()->back();
    }

    public function logOut(){
        Auth::logout();
        return redirect()->route('index');
    }

    public function postLogin(Request $request){
        if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember)){
            return redirect()->route('home');
        }else{
            return redirect()->route('index')->with('error','Invalid Login Credentials');
        }
    }
}
