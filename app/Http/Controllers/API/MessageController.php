<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class MessageController extends Controller
{
    public function index()
    {
        return Message::orderBy('created_at','desc')->paginate(10);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'email' => 'required|string',
            'contact' => 'required|string',
            'body' => 'required|string',
        ]);

        return Message::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'contact' => $request['contact'],
            'body' => $request['body'],
        ]);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $message = Message::findOrFail($id);

        $this->validate($request,[
            'name' => 'required|string',
            'email' => 'required|string',
            'contact' => 'required|string',
            'body' => 'required|string',
        ]);

        $message->update($request->all());
        return $message;
    }


    public function destroy($id)
    {
        $message = Message::findOrFail($id);
        $message->delete();
        return $message;
    }
}
