<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;

class CommentController extends Controller
{
    public function index()
    {
        return Comment::with('project')->latest()->orderBy('created_at')->paginate(10);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'comment' => 'required|string',
            'project_id' => 'required',
        ]);

        return Comment::create([
            'comment' => $request['comment'],
            'project_id' => $request['project_id'],
        ]);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $comment = Comment::findOrFail($id);

        $this->validate($request,[
            'comment' => 'required|string',
        ]);

        $comment->update($request->all());
        return $comment;
    }


    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return $comment;
    }
}
