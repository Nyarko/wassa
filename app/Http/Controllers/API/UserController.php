<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Image;

class UserController extends Controller
{

    public function index()
    {
        return User::latest()->paginate(20);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'contact' => 'required|string',
        ]);
        $picture = '';

        if($request->photo){
            $name = time().'.'. explode('/', explode(':', substr
                ($request->photo, 0, strpos($request->photo,';')))[1])[1];
            Image::make($request->photo)->fit(500,500)->save(public_path('img/users/').$name);
            $picture = $name;
        }else{
            $picture = 'avatar.png';
        }

        return User::create([
           'name' => $request['name'],
           'email' => $request['email'],
           'type' => $request['type'],
           'bio' => $request['bio'],
           'photo' => $picture,
           'contact' => $request['contact'],
           'position' => $request['position'],
           'password'=> Hash::make($request['password']),
        ]);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request,[
          'name' => 'required|string|max:191',
          'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
          'password' => 'sometimes|string|min:8',
          'photo' => 'sometimes|string',
        ]);

        //$current = $user->photo;
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->type = $request['type'];
        $user->bio = $request['bio'];
        $user->contact = $request['contact'];
        $user->position = $request['position'];
        $user->password = Hash::make($request['password']);

        return $request->photo;

        if($request->photo){
            $name = time().'.'. explode('/', explode(':', substr
                ($request->photo, 0, strpos($request->photo,';')))[1])[1];
            Image::make($request->photo)->save(public_path('img/users/').$name);

            //$request->merge(['photo' => $name]);
            $user->photo = $name;
        }

         $user->update();
        //$user->update($request->all());

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $user;
    }

}
