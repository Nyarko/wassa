<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use Image;

class GalleryController extends Controller
{
    public function index()
    {
        return Gallery::with('project')->latest()->orderBy('created_at')->paginate(10);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'project_id' => 'required',
            'photo' => 'required',
        ]);

        if(count($request->photo) > 0){
            foreach ($request->photo as $photo){
                $gallery = new  Gallery();
                $picture = $photo;
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save(public_path('img/projects/'. $filename));

                $gallery->image = $filename;
                $gallery->project_id = $request->project_id;
                $gallery->save();
            }
        }
        return $gallery;
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $gallery = Gallery::findOrFail($id);

        $this->validate($request,[
            'image' => 'required|string',
        ]);

        if($request->photo){
            $name = time().'.'. explode('/', explode(':', substr
                ($request->photo, 0, strpos($request->photo,';')))[1])[1];
            Image::make($request->photo)->fit(400,400)->save(public_path('img/users/').$name);

            $gallery->image = $name;
        }

        $gallery->update();
        return $gallery;
    }


    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->delete();
        return $gallery;
    }
}
