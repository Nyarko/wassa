<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Carousel;
use Intervention\Image\Facades\Image;

class CarouselController extends Controller
{

    public function index()
    {
        return Carousel::latest()->orderBy('created_at')->paginate(10);
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string',
            'description' => 'required|string',
            'photo' => 'required',
        ]);

        $carousel = new Carousel();

        $carousel->title = $request['title'];
        $carousel->description = $request['description'];

        if($request->photo != null){
            $picture = $request->photo;
            $filename = time().$picture->getClientOriginalName();
            Image::make($picture)->save( public_path('img/carousel/'. $filename));

            $carousel->photo = $filename;

        }else {
            $carousel->photo = 'avatar.png';
        }

        $carousel->save();

        return $carousel;
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $carousel = Carousel::findOrFail($id);

        $this->validate($request,[
            'title' => 'required|string',
            'description' => 'required|string',
            'photo' => 'sometimes|string',
        ]);

        $carousel->title = $request['title'];
        $carousel->description = $request['description'];

        if($request->photo != null){
            $name = time().'.'. explode('/', explode(':', substr
                ($request->photo, 0, strpos($request->photo,';')))[1])[1];
            Image::make($request->photo)->save(public_path('img/users/').$name);

            $carousel->photo = $name;
            //$request->merge(['photo' => $name]);
        }

        $carousel->update();
        return $carousel;
    }


    public function destroy($id)
    {
        $carousel = Carousel::findOrFail($id);
        $carousel->delete();
        return $carousel;
    }
}
