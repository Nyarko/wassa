<?php

namespace App\Http\Controllers\API;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Project;
use App\User;
use Image;

class ProjectController extends Controller
{
    public function index()
    {
        return Project::where('user_id','!=',null)->with('user')->orderBy('created_at','desc')->paginate(10);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string|max:191',
            'body' => 'required|string',
            'status' => 'required|string',
        ]);

        $project = new Project();

        $project->title = $request['title'];
        $project->body = $request['body'];
        $project->status = $request['status'];
        $project->user_id = Auth::user()->id;

        if(count($request->photo) > 0){
            foreach ($request->photo as $photo){
                $picture = $photo;
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('img/projects/'. $filename));
                $project->photo = $filename;
            }
        }else {
            $project->photo = 'avatar.png';
        }
        $project->save();
        $project_id = $project->id;


        if(count($request->photo) > 0){
            foreach ($request->photo as $photo){
                $gallery = new  Gallery();
                $picture = $photo;
                $filename = time().$picture->getClientOriginalName();
                Image::make($picture)->save( public_path('img/projects/'. $filename));

                $gallery->image = $filename;
                $gallery->project_id = $project_id;
                $gallery->save();
            }
        } else {
            $gallery = new  Gallery();
            $gallery->image = 'avatar.png';
            $gallery->project_id = $project_id;
            $gallery->save();
        }
        return $gallery;
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {

        $project = Project::findOrFail($id);

        $this->validate($request,[
            'title' => 'required|string',
            'body' => 'required|string',
            'status' => 'required|string',
        ]);

        $project->title = $request['title'];
        $project->body = $request['body'];
        $project->status = $request['status'];
        $project->user_id = Auth::user()->id;

//        if($request->image != null){
//            $picture = $request->image;
//            $filename = time().$picture->getClientOriginalName();
//            Image::make($picture)->fit(300,300)->save( public_path('img/projects/'. $filename));
//            $project->photo = $filename;
//        }


        $project->update();
        return $project;

    }


    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();
        return $project;
    }

    public function projectTitle(){
        $projects = Project::select('id','title')->orderBy('created_at','desc')->latest()->paginate(50);
        return $projects;
    }
}
