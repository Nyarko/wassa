<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $guarded = ['id'];

    public function project(){
        return $this->belongsTo(Project::class,'project_id')->select('title','status','id')->where('deleted_at',null)->groupBy('title','status','id');
    }

    public function getImageAttribute($value){
        return asset('img/projects/'.$value);
    }
}
