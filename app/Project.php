<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function comments(){
        return $this->hasMany(Comment::class)->select('comment','project_id');
    }

    public function user(){
        return $this->belongsTo(User::class)->select('name','id')->where('id','!=',null);
    }

    public function gallery(){
        return $this->hasMany(Gallery::class)->select('image','id');
    }

    public function getPhotoAttribute($value){
        return asset('img/projects/'.$value);
    }
}
