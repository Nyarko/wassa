<?php

Route::get('/', 'PagesController@index')->name('index');
Route::get('/login', 'PagesController@index')->name('login');
Route::get('/all/project','PagesController@ProjectAll')->name('allproject');
Route::get('/all/gallery','PagesController@Gallery')->name('allgallery');
Route::get('/contact','PagesController@Users')->name('profile');
Route::post('/send/message','PagesController@sendMessage')->name('sendmessage');
Route::get('/view/project/{id}','PagesController@viewProject')->name('viewproject');
Route::post('/send/comment', 'PagesController@sendComment')->name('send-comment');

Route::get('logout','PagesController@logOut')->name('logout');


Route::post('/login','PagesController@postLogin')->name('login');

//adding middleware
Route::group(['middleware' => ['auth']], function () {
    Route::resource('/user','API\UserController');
    Route::resource('/project', 'API\ProjectController');
    Route::resource('/carousel','API\CarouselController');
    Route::resource('/comment','API\CommentController');
    Route::resource('/message','API\MessageController');
    Route::resource('/gallery','API\GalleryController');
    Route::get('/title/get','API\ProjectController@projectTitle');
});




//admin route

Route::get('/home', 'HomeController@index')->name('home');
Route::get('{path}',"HomeController@index")->where('path', '([A-z\d-\/_.]+)?');





