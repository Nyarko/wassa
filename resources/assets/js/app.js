window.Vue = require('vue');
require('./myfunctions/bootstrap');
require('./myfunctions/myfun');

//this help in the form validation
import { Form, HasError, AlertError } from 'vform';
import VueRouter from 'vue-router';
import routes from './myfunctions/routes'
import VueProgressBar from 'vue-progressbar';
import Swal from 'sweetalert2';


window.Form = Form;
window.Swal = Swal;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);


//adding vue progressbar dependencies
const options = {
  color: '#38C172',
  failedColor: '#874b4b',
  thickness: '5px',
  transition: {
    speed: '0.5s',
    opacity: '0.6s',
    termination: 300
  },
  autoRevert: true,
  location: 'top',
  inverse: false
}

//alert toast
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

//popup alert
window.Toast = Toast;
//Seting up vue route in laravel
Vue.use(VueRouter);
//progreebar
Vue.use(VueProgressBar, options);

//registering routes
const router = new VueRouter({
  mode: 'history', //help keep history and perevent eg /home#profile
  routes // short for `routes: routes`
});

//fire of waiting
window.Fire = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('carousel', require('./pages/Carousel.vue'));

const app = new Vue({
    el: '#app',
    router  //remember to add router here
});
