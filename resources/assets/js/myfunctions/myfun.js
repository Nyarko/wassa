import moment from 'moment';

//convert words first letter to upper case
Vue.filter('uptext', function(value){
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
});

//global for date human readable
Vue.filter('myDate', function(created){
    return moment(created).format('MMMM ddd YYYY, hh:mm:ss');
});

Vue.filter('myDateOnly', function(created){
    return moment(created).format('MMMM ddd YYYY');
});

//substring in vue
Vue.filter('reduceText', function (text, length, suffix) {
    if (text.length > length){
        return text.substring(0, length) + suffix;
    }else{
        return text;
    }
});
