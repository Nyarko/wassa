import Carousel from '../components/Carousel';
import Dashboard from '../components/Dashboard';
import Users from '../components/Users';
import Comment from '../components/Comment';
import Gallery from '../components/Gallery';
import Message from '../components/Message';
import Projects from '../components/Project';

const routes = [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard
    },
    {
        path: '/users',
        name: 'users',
        component: Users
    },
    {
        path: '/all-comment',
        name: 'comment',
        component: Comment
    },
    {
        path: '/all-gallery',
        name: 'gallery',
        component: Gallery
    },
    {
        path: '/all-carousel',
        name: 'carousel',
        component: Carousel
    },
    {
        path: '/all-message',
        name: 'message',
        component: Message
    },
    {
        path: '/all-project',
        name: 'project',
        component: Projects
    },
    {
        path: '*',
        component: Dashboard
    }
]

export default routes
