@extends('layouts.pages')

@section('section')

    <section class="py-3 bg-white">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">Stay and eat like a local</p>
                    <h2>Our Gallery</h2>
                </div>
            </div>
            <div class="row">

                @foreach($galleries as $gallery)

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 swiper-container">
                        <div class="swiper-wrapper pb-5">

                            <div class="swiper-slide h-auto px-2">
                                <div class="card card-poster gradient-overlay mb-4 mb-lg-0">
                                    <a href="category.html" class="tile-link"></a>
                                    <img src="{{$gallery->image}}" alt="Card image" class="bg-image">
                                    <div class="card-body overlay-content">
                                        <h6 class="card-title text-shadow text-uppercase text-xs">{{ $gallery->project->title }}</h6>
                                        <p class="card-text text-sm">
                                            <i class="fa fa-stamp text-success"></i> <b>{{ $gallery->project->status }}</b>
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                @endforeach

            </div>
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-0"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <div class="pagination justify-content-center pagination-lg">
                        {{ $galleries->links() }}
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-0"></div>
            </div>
        </div>
    </section>

@endsection