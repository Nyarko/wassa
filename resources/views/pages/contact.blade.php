@extends('layouts.pages')

@section('section')

    <section class="py-3 bg-gray-100">
        <div class="container">

            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">Contact and profile of leaders</p>
                    <h2>Profile Information</h2>
                </div>
            </div>
            <!-- Additional required wrapper-->
            <div class="row">

                @foreach($users as $user)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 swiper-container mb-5">

                        <div class="card h-100 border-0 shadow">
                            <div class="card-img-top overflow-hidden gradient-overlay">
                                <img src="{{$user->photo}}" alt="Modern, Well-Appointed Room" class="img img-responsive" width="100%"/>
                                <div class="card-img-overlay-bottom z-index-20">
                                    <div class="media text-white text-sm align-items-center"><img src="{{$user->photo}}" alt="Pamela" class="avatar avatar-border-white mr-2"/>
                                        <div class="media-body"><strong>{{$user->name}}</strong></div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body d-flex align-items-center">
                                <div class="w-100">
                                    <h6 class="card-title">
                                        <i class="fa fa-envelope"></i>
                                        <a class="text-decoration-none text-dark">{{ $user->email }}</a>
                                    </h6>
                                    <div class="d-flex card-subtitle mb-3">
                                        <p class="flex-grow-1 mb-0 text-muted text-sm">Position: {{ $user->position}}</p>

                                    </div>
                                    <p class="card-text text-muted"><span class="h4 text-primary"> <i class="fa fa-phone"></i> {{ $user->contact}}</span></p>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach

            </div>

            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-0"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <div class="pagination justify-content-center pagination-lg">
                        {{ $users->links() }}
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-0"></div>
            </div>

        </div>
    </section>

@endsection