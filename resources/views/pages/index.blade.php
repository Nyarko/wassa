@extends('layouts.pages')

@section('section')

    @include('pages.carousel')
    <section class="py-3 bg-gray-100">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">Project in the Municipal</p>
                    <h2>On Going Projects</h2>
                </div>
            </div>
            <div class="row mb-5">
                <!-- blog item-->

                @foreach($projects as $project)
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <div class="card shadow border-0 h-100">
                            <a href="{{route('viewproject',[$project->id])}}">
                                <img src="{{$project->photo}}" alt="..." class="img-fluid card-img-top"/>
                            </a>
                            <div class="card-body">
                                <a href="{{route('viewproject',[$project->id])}}" class="text-uppercase text-muted text-sm">Posted By : {{$project->user->name}}</a>
                                <p class="text-gray-500 text-sm my-3 text-left">
                                    <span class="text-left"><i class="far fa-clock"></i>{{ date('M j, Y', strtotime($project->created_at))}}</span>
                                    <span class="float-right text-success" style="margin-left: 100px"><i class="fa fa-stamp"></i> {{$project->status }}</span>
                                </p>
                                <h6>
                                    <a href="{{route('viewproject',[$project->id])}}" class="text-dark">{{ $project->title}}</a>
                                </h6>
                                <p class="my-2 text-muted text-sm">
                                    {{ str_limit($project->body, $limit = 150, $end = '...') }}
                                </p>
                                <a href="{{route('viewproject',[$project->id])}}" class="btn btn-primary btn-xs">read</a>
                                <span class="float-right">
                                    <i class="fa fa-comment"></i> {{$project->comments_count}} Comments
                                </span>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>



            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">Project in the Municipal</p>
                    <h2>Galleries of Projects</h2>
                </div>
            </div>
            <div class="row">
                <div class="swiper-container guides-slider">
                    <!-- Additional required wrapper-->
                    <div class="swiper-wrapper pb-5">
                        <!-- Slides-->

                        @foreach($galleries as $gallery)
                            <div class="swiper-slide h-auto w-25 px-2">
                                <div class="card card-poster gradient-overlay mb-4 mb-lg-0">
                                    <img src="{{$gallery->image}}" alt="Card image" class="bg-image">
                                    <div class="card-body overlay-content">
                                        <h6 class="card-title text-shadow text-uppercase text-xs">{{ $gallery->project->title }}</h6>
                                        <p class="card-text text-sm">
                                            <i class="fa fa-stamp text-success"></i> <b>{{ $gallery->project->status }}</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                </div>
            </div>

        </div>
    </section>

@endsection