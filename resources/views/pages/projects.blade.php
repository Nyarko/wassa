@extends('layouts.pages')

@section('section')

    <section class="py-3 bg-gray-100">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-12">
                    <p class="subtitle text-secondary text-center">Project in the Municipal</p>
                    <h2 class="text-center">ALL PROJECTS AND THEIR STATUS</h2>
                </div>
            </div>

            <div class="row mb-5">
                <!-- blog item-->

                @foreach($projects as $project)

                    <div class="col-lg-4 col-sm-6 mb-4">
                        <div class="card shadow border-0 h-100">
                            <a href="post.html">
                                <img src="{{$project->photo}}" alt="..." class="img-fluid card-img-top"/>
                            </a>
                            <div class="card-body">
                                <a href="#" class="text-uppercase text-muted text-sm">Posted By : {{ $project->user->name }}</a>
                                <p class="text-gray-500 text-sm my-3 text-left">
                                    <span class="text-left">
                                        <i class="far fa-clock"></i>{{ date('M j, Y', strtotime($project->created_at))}}
                                    </span>
                                    <span class="float-right text-success" style="margin-left: 100px">
                                        <i class="fa fa-stamp"></i> {{ $project->status }}
                                    </span>

                                </p>
                                <p>
                                <h6>
                                    <a href="post.html" class="text-dark">{{$project->title}}</a>
                                </h6>
                                </p>
                                <p class="my-2 text-muted text-sm">
                                    {{ str_limit($project->body, $limit = 150, $end = '...') }}
                                </p>
                                <p>
                                    <a href="{{route('viewproject',[$project->id])}}" class="btn btn-primary">read</a>
                                    <span class="float-right">
                                       <i class="fa fa-comment"></i> {{$project->comments_count}} Comments
                                    </span>
                                </p>
                            </div>
                        </div>
                    </div>

                @endforeach


            </div>

            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-0"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <div class="pagination justify-content-center pagination-lg">
                        {{ $projects->links() }}
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-0"></div>
            </div>

        </div>
    </section>

@endsection