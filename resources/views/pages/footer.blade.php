<!-- Footer-->
<footer class="position-relative z-index-10 d-print-none">
  <!-- Main block - menus, subscribe form-->
  <div class="py-4 bg-gray-200 text-muted">
    <div class="container">
      <div class="row">

        <div class="col-lg-6 mb-6 mb-lg-0">
          <div class="font-weight-bold text-uppercase text-dark mb-3">Social Media Links</div>
          <p>Contact us on the below social media links</p>
          <ul class="list-inline">
            <li class="list-inline-item"><a href="#" target="_blank" title="twitter" class="text-muted text-hover-primary"><i class="fab fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="facebook" class="text-muted text-hover-primary"><i class="fab fa-facebook"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="instagram" class="text-muted text-hover-primary"><i class="fab fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="pinterest" class="text-muted text-hover-primary"><i class="fab fa-pinterest"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="vimeo" class="text-muted text-hover-primary"><i class="fab fa-vimeo"></i></a></li>
          </ul>
        </div>

        <div class="col-lg-6">

          @include('partials.alerts')

          <h6 class="text-uppercase text-dark mb-3">Send Us Message</h6>

          <form class="form" action="{{route('sendmessage')}}" method="post">
            {{ csrf_field() }}
            <div class="input-group mb-3">
              <input type="text" name="name" placeholder="Enter Your Fullname" class="form-control bg-transparent border-dark border-right-0" required>
              <div class="input-group-append">
                <span class="btn btn-outline-dark border-left-0">
                  <i class="fa fa-user text-lg"></i>
                </span>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="email" name="email" placeholder="Enter Your Email Address" class="form-control bg-transparent border-dark border-right-0" required>
              <div class="input-group-append">
                <span class="btn btn-outline-dark border-left-0">
                  <i class="fa fa-envelope text-lg"></i>
                </span>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="text" name="contact" placeholder="Enter Your Contact Number" class="form-control bg-transparent border-dark border-right-0" required>
              <div class="input-group-append">
                <span class="btn btn-outline-dark border-left-0">
                  <i class="fa fa-phone text-lg"></i>
                </span>
              </div>
            </div>

            <div class="input-group mb-3">
              <textarea class="form-control bg-transparent border-dark" name="body" rows="6" cols="10" placeholder="Write your message here!" style="resize:none" required></textarea>
            </div>

            <div class="input-group">
              <button type="submit" name="button" class="btn btn-dark"> <i class="fa fa-paper-plane"></i> Send</button>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
  <!-- Copyright section of the footer-->
  <div class="py-3 font-weight-light bg-gray-800 text-gray-300">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6 text-center">
          <p class="text-sm mb-md-0">&copy; 2019 WASSA.  All rights reserved.</p>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- /Footer end-->
