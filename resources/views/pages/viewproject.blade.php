@extends('layouts.pages')

@section('section')

    <section class="py-3 bg-gray-100">
        <div class="container">

            <div class="row">
                <!-- blog item-->
                    <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12">
                        <div class="row mb-2">
                        <div class="card shadow border-0 h-100 w-100">

                            <div class="card-header">
                                <div class="swiper-container guides-slider">
                                    <div class="swiper-wrapper pb-5">
                                        @foreach($galleries as $gallery)
                                        <div class="swiper-slide h-auto w-50 px-1">
                                            <div class="card card-poster gradient-overlay mb-4 mb-lg-0">
                                             <img src="{{$gallery->image}}" alt="Card image" class="bg-image">
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <a class="text-uppercase text-muted text-sm">Posted By : {{ $project->user->name }}</a>
                                <p class="text-gray-500 text-sm my-3 text-left">
                                    <span class="text-left"><i class="far fa-clock"></i> {{ date('M j, Y', strtotime($project->created_at))}}</span>
                                    <span class="text-right text-success float-right"><i class="fa fa-stamp"></i> {{ $project->status }}</span>
                                </p>
                                <h6>
                                    <a class="text-dark">{{$project->title}}</a>
                                </h6>
                                <p class="my-2 text-muted text-sm">
                                    {{ $project->body}}
                                </p>
                            </div>

                            <div class="card-footer">
                                <div class="swiper-container">
                                    <form class="form" action="{{route('send-comment')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="project_id" value="{{$project->id}}">
                                        <div class="form-group">
                                            <label for="comment"><span class="float-left mr-2"><i class="fa fa-comment"></i> {{ count($comments) }} </span>  Comments </label>
                                            <textarea class="form-control" name="comment" id="comment" cols="10" rows="4" style="resize:none" placeholder="Enter your comment"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-success float-right" type="submit"><i class="fa fa-comment"></i> Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                         </div>
                        </div>

                        <div class="row">
                            <div class="card shadow w-100">
                               <p>
                                <ul style="list-style-type:none">
                                    @if (count($comments) > 0)
                                        @foreach($comments as $comment)
                                        <li style="margin-left:-20px">
                                            <i class="fa fa-comment text-primary"></i> {{$comment->comment}}
                                        </li>
                                        @endforeach
                                    @else
                                        <li style="margin-left:-20px">
                                            No Comments
                                        </li>
                                    @endif
                                </ul>
                               </p>
                            </div>
                        </div>

                    </div>

                   <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                       <div class="row mb-1">
                           <h2 style="text-align: center">Latest Projects</h2>
                       </div>
                       @foreach($projects as $project)
                         <div class="row mb-3">
                               <div class="card shadow border-0 h-100 w-100">
                                   <div class="card-body">
                                       <h6>
                                           <a href="{{route('viewproject',[$project->id])}}" class="text-dark">{{$project->title}}</a>
                                       </h6>
                                       <p class="my-2 text-muted text-sm">
                                           {{ str_limit($project->body, $limit = 100, $end = '...') }}
                                       </p>
                                       <a href="{{route('viewproject',[$project->id])}}">read</a>
                                   </div>
                               </div>
                         </div>
                       @endforeach

                   </div>

            </div>

        </div>
    </section>

@endsection