<section class="py-6 bg-gray-100">
  <div class="container">
    <div class="text-center pb-lg-4">
      <p class="subtitle text-secondary">One-of-a-kind vacation rentals </p>
      <h2 class="mb-5">Booking with us is easy</h2>
    </div>
    <div class="row">
      <div class="col-lg-4 mb-3 mb-lg-0 text-center">
        <div class="px-0 px-lg-3">
          <div class="icon-rounded bg-primary-light mb-3">
            <svg class="svg-icon text-primary w-2rem h-2rem">
              <use xlink:href="#destination-map-1"> </use>
            </svg>
          </div>
          <h3 class="h5">Find the perfect rental</h3>
          <p class="text-muted">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed in</p>
        </div>
      </div>
      <div class="col-lg-4 mb-3 mb-lg-0 text-center">
        <div class="px-0 px-lg-3">
          <div class="icon-rounded bg-primary-light mb-3">
            <svg class="svg-icon text-primary w-2rem h-2rem">
              <use xlink:href="#pay-by-card-1"> </use>
            </svg>
          </div>
          <h3 class="h5">Book with confidence</h3>
          <p class="text-muted">The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pit</p>
        </div>
      </div>
      <div class="col-lg-4 mb-3 mb-lg-0 text-center">
        <div class="px-0 px-lg-3">
          <div class="icon-rounded bg-primary-light mb-3">
            <svg class="svg-icon text-primary w-2rem h-2rem">
              <use xlink:href="#heart-1"> </use>
            </svg>
          </div>
          <h3 class="h5">Enjoy your vacation</h3>
          <p class="text-muted">His room, a proper human room although a little too small, lay peacefully between its four familiar </p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="py-6 bg-white">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-8">
        <p class="subtitle text-primary">Stay and eat like a local</p>
        <h2>Our guides</h2>
      </div>
      <div class="col-md-4 d-lg-flex align-items-center justify-content-end"><a href="category.html" class="text-muted text-sm">

          See all guides<i class="fas fa-angle-double-right ml-2"></i></a></div>
    </div>
    <div class="row">
      <div class="swiper-container guides-slider">
        <!-- Additional required wrapper-->
        <div class="swiper-wrapper pb-5">
          <!-- Slides-->
          <div class="swiper-slide h-auto px-2">
            <div class="card card-poster gradient-overlay mb-4 mb-lg-0"><a href="category.html" class="tile-link"></a><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/new-york.jpg" alt="Card image" class="bg-image">
              <div class="card-body overlay-content">
                <h6 class="card-title text-shadow text-uppercase">New York</h6>
                <p class="card-text text-sm">The big apple</p>
              </div>
            </div>
          </div>
          <div class="swiper-slide h-auto px-2">
            <div class="card card-poster gradient-overlay mb-4 mb-lg-0"><a href="category.html" class="tile-link"></a><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/paris.jpg" alt="Card image" class="bg-image">
              <div class="card-body overlay-content">
                <h6 class="card-title text-shadow text-uppercase">Paris</h6>
                <p class="card-text text-sm">Artist capital of Europe</p>
              </div>
            </div>
          </div>
          <div class="swiper-slide h-auto px-2">
            <div class="card card-poster gradient-overlay mb-4 mb-lg-0"><a href="category.html" class="tile-link"></a><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/barcelona.jpg" alt="Card image" class="bg-image">
              <div class="card-body overlay-content">
                <h6 class="card-title text-shadow text-uppercase">Barcelona</h6>
                <p class="card-text text-sm">Dalí, Gaudí, Barrio Gotico</p>
              </div>
            </div>
          </div>
          <div class="swiper-slide h-auto px-2">
            <div class="card card-poster gradient-overlay mb-4 mb-lg-0"><a href="category.html" class="tile-link"></a><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/prague.jpg" alt="Card image" class="bg-image">
              <div class="card-body overlay-content">
                <h6 class="card-title text-shadow text-uppercase">Prague</h6>
                <p class="card-text text-sm">City of hundred towers</p>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-pagination d-md-none"> </div>
      </div>
    </div>
  </div>
</section>
<section class="py-6 bg-gray-100">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-8">
        <p class="subtitle text-secondary">Hurry up, these are expiring soon.        </p>
        <h2>Last minute deals</h2>
      </div>
      <div class="col-md-4 d-lg-flex align-items-center justify-content-end"><a href="category.html" class="text-muted text-sm">

          See all deals<i class="fas fa-angle-double-right ml-2"></i></a></div>
    </div>
    <!-- Slider main container-->
    <div data-swiper="{&quot;slidesPerView&quot;:4,&quot;spaceBetween&quot;:20,&quot;loop&quot;:true,&quot;roundLengths&quot;:true,&quot;breakpoints&quot;:{&quot;1200&quot;:{&quot;slidesPerView&quot;:3},&quot;991&quot;:{&quot;slidesPerView&quot;:2},&quot;565&quot;:{&quot;slidesPerView&quot;:1}},&quot;pagination&quot;:{&quot;el&quot;:&quot;.swiper-pagination&quot;,&quot;clickable&quot;:true,&quot;dynamicBullets&quot;:true}}" class="swiper-container swiper-container-mx-negative swiper-init">
      <!-- Additional required wrapper-->
      <div class="swiper-wrapper pb-5">
        <!-- Slides-->
        <div class="swiper-slide h-auto px-2">
          <!-- place item-->
          <div data-marker-id="59c0c8e33b1527bfe2abaf92" class="w-100 h-100">
            <div class="card h-100 border-0 shadow">
              <div class="card-img-top overflow-hidden gradient-overlay"> <img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1484154218962-a197022b5858.jpg" alt="Modern, Well-Appointed Room" class="img-fluid"/><a href="detail-rooms.html" class="tile-link"></a>
                <div class="card-img-overlay-bottom z-index-20">
                  <div class="media text-white text-sm align-items-center"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-0.jpg" alt="Pamela" class="avatar avatar-border-white mr-2"/>
                    <div class="media-body">Pamela</div>
                  </div>
                </div>
                <div class="card-img-overlay-top text-right"><a href="javascript: void();" class="card-fav-icon position-relative z-index-40">
                    <svg class="svg-icon text-white">
                      <use xlink:href="#heart-1"> </use>
                    </svg></a></div>
              </div>
              <div class="card-body d-flex align-items-center">
                <div class="w-100">
                  <h6 class="card-title"><a href="detail-rooms.html" class="text-decoration-none text-dark">Modern, Well-Appointed Room</a></h6>
                  <div class="d-flex card-subtitle mb-3">
                    <p class="flex-grow-1 mb-0 text-muted text-sm">Private room</p>
                    <p class="flex-shrink-1 mb-0 card-stars text-xs text-right"><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i>
                    </p>
                  </div>
                  <p class="card-text text-muted"><span class="h4 text-primary">$80</span> per night</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide h-auto px-2">
          <!-- place item-->
          <div data-marker-id="59c0c8e322f3375db4d89128" class="w-100 h-100">
            <div class="card h-100 border-0 shadow">
              <div class="card-img-top overflow-hidden gradient-overlay"> <img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1426122402199-be02db90eb90.jpg" alt="Cute Quirky Garden apt, NYC adjacent" class="img-fluid"/><a href="detail-rooms.html" class="tile-link"></a>
                <div class="card-img-overlay-bottom z-index-20">
                  <div class="media text-white text-sm align-items-center"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-7.jpg" alt="John" class="avatar avatar-border-white mr-2"/>
                    <div class="media-body">John</div>
                  </div>
                </div>
                <div class="card-img-overlay-top text-right"><a href="javascript: void();" class="card-fav-icon position-relative z-index-40">
                    <svg class="svg-icon text-white">
                      <use xlink:href="#heart-1"> </use>
                    </svg></a></div>
              </div>
              <div class="card-body d-flex align-items-center">
                <div class="w-100">
                  <h6 class="card-title"><a href="detail-rooms.html" class="text-decoration-none text-dark">Cute Quirky Garden apt, NYC adjacent</a></h6>
                  <div class="d-flex card-subtitle mb-3">
                    <p class="flex-grow-1 mb-0 text-muted text-sm">Entire apartment</p>
                    <p class="flex-shrink-1 mb-0 card-stars text-xs text-right"><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-gray-300">                                  </i>
                    </p>
                  </div>
                  <p class="card-text text-muted"><span class="h4 text-primary">$121</span> per night</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide h-auto px-2">
          <!-- place item-->
          <div data-marker-id="59c0c8e3a31e62979bf147c9" class="w-100 h-100">
            <div class="card h-100 border-0 shadow">
              <div class="card-img-top overflow-hidden gradient-overlay"> <img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1512917774080-9991f1c4c750.jpg" alt="Modern Apt - Vibrant Neighborhood!" class="img-fluid"/><a href="detail-rooms.html" class="tile-link"></a>
                <div class="card-img-overlay-bottom z-index-20">
                  <div class="media text-white text-sm align-items-center"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-8.jpg" alt="Julie" class="avatar avatar-border-white mr-2"/>
                    <div class="media-body">Julie</div>
                  </div>
                </div>
                <div class="card-img-overlay-top text-right"><a href="javascript: void();" class="card-fav-icon position-relative z-index-40">
                    <svg class="svg-icon text-white">
                      <use xlink:href="#heart-1"> </use>
                    </svg></a></div>
              </div>
              <div class="card-body d-flex align-items-center">
                <div class="w-100">
                  <h6 class="card-title"><a href="detail-rooms.html" class="text-decoration-none text-dark">Modern Apt - Vibrant Neighborhood!</a></h6>
                  <div class="d-flex card-subtitle mb-3">
                    <p class="flex-grow-1 mb-0 text-muted text-sm">Entire apartment</p>
                    <p class="flex-shrink-1 mb-0 card-stars text-xs text-right"><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-gray-300">                                  </i><i class="fa fa-star text-gray-300">                                  </i>
                    </p>
                  </div>
                  <p class="card-text text-muted"><span class="h4 text-primary">$75</span> per night</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide h-auto px-2">
          <!-- place item-->
          <div data-marker-id="59c0c8e3503eb77d487e8082" class="w-100 h-100">
            <div class="card h-100 border-0 shadow">
              <div class="card-img-top overflow-hidden gradient-overlay"> <img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1494526585095-c41746248156.jpg" alt="Sunny Private Studio-Apartment" class="img-fluid"/><a href="detail-rooms.html" class="tile-link"></a>
                <div class="card-img-overlay-bottom z-index-20">
                  <div class="media text-white text-sm align-items-center"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-9.jpg" alt="Barbora" class="avatar avatar-border-white mr-2"/>
                    <div class="media-body">Barbora</div>
                  </div>
                </div>
                <div class="card-img-overlay-top text-right"><a href="javascript: void();" class="card-fav-icon position-relative z-index-40">
                    <svg class="svg-icon text-white">
                      <use xlink:href="#heart-1"> </use>
                    </svg></a></div>
              </div>
              <div class="card-body d-flex align-items-center">
                <div class="w-100">
                  <h6 class="card-title"><a href="detail-rooms.html" class="text-decoration-none text-dark">Sunny Private Studio-Apartment</a></h6>
                  <div class="d-flex card-subtitle mb-3">
                    <p class="flex-grow-1 mb-0 text-muted text-sm">Shared room</p>
                    <p class="flex-shrink-1 mb-0 card-stars text-xs text-right"><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-gray-300">                                  </i>
                    </p>
                  </div>
                  <p class="card-text text-muted"><span class="h4 text-primary">$93</span> per night</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide h-auto px-2">
          <!-- place item-->
          <div data-marker-id="59c0c8e39aa2eed0626e485d" class="w-100 h-100">
            <div class="card h-100 border-0 shadow">
              <div class="card-img-top overflow-hidden gradient-overlay"> <img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1522771739844-6a9f6d5f14af.jpg" alt="Mid-Century Modern Garden Paradise" class="img-fluid"/><a href="detail-rooms.html" class="tile-link"></a>
                <div class="card-img-overlay-bottom z-index-20">
                  <div class="media text-white text-sm align-items-center"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-10.jpg" alt="Jack" class="avatar avatar-border-white mr-2"/>
                    <div class="media-body">Jack</div>
                  </div>
                </div>
                <div class="card-img-overlay-top text-right"><a href="javascript: void();" class="card-fav-icon position-relative z-index-40">
                    <svg class="svg-icon text-white">
                      <use xlink:href="#heart-1"> </use>
                    </svg></a></div>
              </div>
              <div class="card-body d-flex align-items-center">
                <div class="w-100">
                  <h6 class="card-title"><a href="detail-rooms.html" class="text-decoration-none text-dark">Mid-Century Modern Garden Paradise</a></h6>
                  <div class="d-flex card-subtitle mb-3">
                    <p class="flex-grow-1 mb-0 text-muted text-sm">Entire house</p>
                    <p class="flex-shrink-1 mb-0 card-stars text-xs text-right"><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i>
                    </p>
                  </div>
                  <p class="card-text text-muted"><span class="h4 text-primary">$115</span> per night</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="swiper-slide h-auto px-2">
          <!-- place item-->
          <div data-marker-id="59c0c8e39aa2edasd626e485d" class="w-100 h-100">
            <div class="card h-100 border-0 shadow">
              <div class="card-img-top overflow-hidden gradient-overlay"> <img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1488805990569-3c9e1d76d51c.jpg" alt="Brooklyn Life, Easy to Manhattan" class="img-fluid"/><a href="detail-rooms.html" class="tile-link"></a>
                <div class="card-img-overlay-bottom z-index-20">
                  <div class="media text-white text-sm align-items-center"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-11.jpg" alt="Stuart" class="avatar avatar-border-white mr-2"/>
                    <div class="media-body">Stuart</div>
                  </div>
                </div>
                <div class="card-img-overlay-top text-right"><a href="javascript: void();" class="card-fav-icon position-relative z-index-40">
                    <svg class="svg-icon text-white">
                      <use xlink:href="#heart-1"> </use>
                    </svg></a></div>
              </div>
              <div class="card-body d-flex align-items-center">
                <div class="w-100">
                  <h6 class="card-title"><a href="detail-rooms.html" class="text-decoration-none text-dark">Brooklyn Life, Easy to Manhattan</a></h6>
                  <div class="d-flex card-subtitle mb-3">
                    <p class="flex-grow-1 mb-0 text-muted text-sm">Private room</p>
                    <p class="flex-shrink-1 mb-0 card-stars text-xs text-right"><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-warning"></i><i class="fa fa-star text-gray-300">                                  </i>
                    </p>
                  </div>
                  <p class="card-text text-muted"><span class="h4 text-primary">$123</span> per night</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- If we need pagination-->
      <div class="swiper-pagination"></div>
    </div>
  </div>
</section>
<!-- Divider Section-->
<section class="py-7 position-relative dark-overlay"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1497436072909-60f360e1d4b1.jpg" alt="" class="bg-image">
  <div class="container">
    <div class="overlay-content text-white py-lg-5">
      <h3 class="display-3 font-weight-bold text-serif text-shadow mb-5">Ready for your next holidays?</h3><a href="category-rooms.html" class="btn btn-light">Get started</a>
    </div>
  </div>
</section>
<section class="py-7">
  <div class="container">
    <div class="text-center">
      <p class="subtitle text-primary">Testimonials</p>
      <h2 class="mb-5">Our dear customers said about us</h2>
    </div>
    <!-- Slider main container-->
    <div class="swiper-container testimonials-slider testimonials">
      <!-- Additional required wrapper-->
      <div class="swiper-wrapper pt-2 pb-5">
        <!-- Slides-->
        <div class="swiper-slide px-3">
          <div class="testimonial card rounded-lg shadow border-0">
            <div class="testimonial-avatar"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-3.jpg" alt="..." class="img-fluid"></div>
            <div class="text">
              <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
              <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
            </div>
          </div>
        </div>
        <div class="swiper-slide px-3">
          <div class="testimonial card rounded-lg shadow border-0">
            <div class="testimonial-avatar"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-3.jpg" alt="..." class="img-fluid"></div>
            <div class="text">
              <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
              <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
            </div>
          </div>
        </div>
        <div class="swiper-slide px-3">
          <div class="testimonial card rounded-lg shadow border-0">
            <div class="testimonial-avatar"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-3.jpg" alt="..." class="img-fluid"></div>
            <div class="text">
              <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
              <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
            </div>
          </div>
        </div>
        <div class="swiper-slide px-3">
          <div class="testimonial card rounded-lg shadow border-0">
            <div class="testimonial-avatar"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-3.jpg" alt="..." class="img-fluid"></div>
            <div class="text">
              <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
              <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
            </div>
          </div>
        </div>
        <div class="swiper-slide px-3">
          <div class="testimonial card rounded-lg shadow border-0">
            <div class="testimonial-avatar"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-3.jpg" alt="..." class="img-fluid"></div>
            <div class="text">
              <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
              <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
            </div>
          </div>
        </div>
        <div class="swiper-slide px-3">
          <div class="testimonial card rounded-lg shadow border-0">
            <div class="testimonial-avatar"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/avatar/avatar-3.jpg" alt="..." class="img-fluid"></div>
            <div class="text">
              <div class="testimonial-quote"><i class="fas fa-quote-right"></i></div>
              <p class="testimonial-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p><strong>Jessica Watson</strong>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-pagination"></div>
    </div>
  </div>
</section>
<section class="py-6 bg-gray-100">
  <div class="container">
    <div class="row mb-5">
      <div class="col-md-8">
        <p class="subtitle text-secondary">Stories from around the globe</p>
        <h2>From our travel blog</h2>
      </div>
      <div class="col-md-4 d-md-flex align-items-center justify-content-end"><a href="blog.html" class="text-muted text-sm">

          See all articles<i class="fas fa-angle-double-right ml-2"></i></a></div>
    </div>
    <div class="row">
      <!-- blog item-->
      <div class="col-lg-4 col-sm-6 mb-4">
        <div class="card shadow border-0 h-100"><a href="post.html"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1512917774080-9991f1c4c750.jpg" alt="..." class="img-fluid card-img-top"/></a>
          <div class="card-body"><a href="#" class="text-uppercase text-muted text-sm letter-spacing-2">Travel </a>
            <h5 class="my-2"><a href="post.html" class="text-dark">Autumn fashion tips and tricks          </a></h5>
            <p class="text-gray-500 text-sm my-3"><i class="far fa-clock mr-2"></i>January 16, 2016</p>
            <p class="my-2 text-muted text-sm">Pellentesque habitant morbi tristique senectus. Vestibulum tortor quam, feugiat vitae, ultricies ege...</p><a href="post.html" class="btn btn-link pl-0">Read more<i class="fa fa-long-arrow-alt-right ml-2"></i></a>
          </div>
        </div>
      </div>
      <!-- blog item-->
      <div class="col-lg-4 col-sm-6 mb-4">
        <div class="card shadow border-0 h-100"><a href="post.html"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1522771739844-6a9f6d5f14af.jpg" alt="..." class="img-fluid card-img-top"/></a>
          <div class="card-body"><a href="#" class="text-uppercase text-muted text-sm letter-spacing-2">Living </a>
            <h5 class="my-2"><a href="post.html" class="text-dark">Newest photo apps          </a></h5>
            <p class="text-gray-500 text-sm my-3"><i class="far fa-clock mr-2"></i>January 16, 2016</p>
            <p class="my-2 text-muted text-sm">ellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibu...</p><a href="post.html" class="btn btn-link pl-0">Read more<i class="fa fa-long-arrow-alt-right ml-2"></i></a>
          </div>
        </div>
      </div>
      <!-- blog item-->
      <div class="col-lg-4 col-sm-6 mb-4">
        <div class="card shadow border-0 h-100"><a href="post.html"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/photo/photo-1482463084673-98272196658a.jpg" alt="..." class="img-fluid card-img-top"/></a>
          <div class="card-body"><a href="#" class="text-uppercase text-muted text-sm letter-spacing-2">Travel </a>
            <h5 class="my-2"><a href="post.html" class="text-dark">Best books about Photography          </a></h5>
            <p class="text-gray-500 text-sm my-3"><i class="far fa-clock mr-2"></i>January 16, 2016</p>
            <p class="my-2 text-muted text-sm">Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.  Mauris placerat eleif...</p><a href="post.html" class="btn btn-link pl-0">Read more<i class="fa fa-long-arrow-alt-right ml-2"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Instagram-->
<section>
  <div class="container-fluid px-0">
    <div class="swiper-container instagram-slider">
      <div class="swiper-wrapper">
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-1.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-2.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-3.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-4.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-5.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-6.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-7.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-8.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-9.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-10.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-11.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-12.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-13.jpg" alt="" class="img-fluid hover-scale"></a></div>
        <div class="swiper-slide overflow-hidden"><a href="#"><img src="https://d19m59y37dris4.cloudfront.net/directory/1-2/img/instagram/instagram-14.jpg" alt="" class="img-fluid hover-scale"></a></div>
      </div>
    </div>
  </div>
</section>
