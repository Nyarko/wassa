<header class="header">
  <style>
    .navbar-nav li a::inactive-selection{
      background-color: red !important;
    }
  </style>
  <!-- Navbar-->
  <nav class="navbar navbar-expand-lg fixed-top shadow navbar-light bg-white">
    <div class="container-fluid">
      <div class="d-flex align-items-center">
       <router-link to="/" class="navbar-brand py-1">
          <img src="{{asset('../img/cups.png')}}" class="img-fluid" alt="logo"
        height="40px" width="40px">
        WASSA
      </router-link>
      </div>
      <button type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><i class="fa fa-bars"></i></button>
      <!-- Navbar Collapse -->
      <div id="navbarCollapse" class="collapse navbar-collapse">
        <form action="#" id="searchcollapsed" class="form-inline mt-4 mb-2 d-sm-none">
          <div class="input-label-absolute input-label-absolute-left input-reset w-100">
            <label for="searchcollapsed_search" class="label-absolute"><i class="fa fa-search"></i><span class="sr-only">What are you looking for?</span></label>
            <input id="searchcollapsed_search" placeholder="Search" aria-label="Search" class="form-control form-control-sm border-0 shadow-0 bg-gray-200">
            <button type="reset" class="btn btn-reset btn-sm"><i class="fa-times fas">           </i></button>
          </div>
        </form>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a href="{{route('index')}}" class="nav-link"><i class="fa fa-home text-primary"></i> Home</a>
          </li>

          <li class="nav-item">
            <a href="{{route('allproject')}}" class="nav-link"><i class="fa fa-tasks text-secondary"></i> Projects</a>
          </li>

          <li class="nav-item">
            <a href="{{route('allgallery')}}" class="nav-link"><i class="fa fa-image text-info"></i> Gallery</a>
          </li>

          <li class="nav-item">
            <a href="{{route('profile')}}" class="nav-link"><i class="fa fa-user text-warning"></i> Contact</a>
          </li>

          <li><a href="" class="nav-link" data-toggle="modal" data-target="#myModal">
              <i class="fa fa-sign-in-alt"></i> Login</a>
          </li>

        </ul>
      </div>
    </div>
  </nav>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center">Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{route('login')}}" method="post">
          {{ csrf_field() }}

          <div class="modal-body">
            <div class="form-group">
              <label><i class="fa fa-envelope"></i> Email</label>
              <input type="email" name="email" placeholder="Enter your email address" class="form-control">
            </div>

            <div class="form-group">
              <label><i class="fa fa-lock"></i> Password</label>
              <input type="password" name="password" placeholder="Enter your password" class="form-control">
            </div>

            <div class="form-group">
              <input type="checkbox" name="chk" id="chk"> Show Password
            </div>

            <div class="form-group">
              <button type="submit" name="button" class="btn btn-success"><i class="fa fa-sign-in-alt"></i> Login</button>
            </div>

          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- /Navbar -->
</header>
<script>
  $(document).ready(function(){

  });
</script>


