<section class="hero-home">
  <div class="swiper-container hero-slider">
    <div class="swiper-wrapper dark-overlay">

       @foreach ($carousels as $carousel)
         <div style="background-image:url({{ $carousel->photo }})" class="swiper-slide">
           <div class="container py-6 py-md-7 text-white z-index-20">
             <div class="row">
               <div class="col-xl-12">
                 <div class="text-center text-lg-left">
                   <p class="subtitle letter-spacing-2 mb-2 text-primary text-shadow">{{$carousel->description}}</p>
                   <h1 class="display-3 font-weight-bold text-shadow text-md">{{$carousel->title}}</h1>
                 </div>
               </div>
             </div>
           </div>
         </div>
       @endforeach

    </div>
  </div>

</section>
