<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  <div class="float-center d-none d-sm-inline">
    <strong><a href="{{route('home')}}">InkCodeKid</a></strong> All rights reserved.
  </div>
  <!-- Default to the left -->
</footer>
