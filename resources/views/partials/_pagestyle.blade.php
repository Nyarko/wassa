<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>My Website</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="all,follow">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Price Slider Stylesheets -->
<link rel="stylesheet" href="{{asset('template/css/slider.css')}}">
<!-- Google fonts - Playfair Display-->
<link rel="stylesheet" href="{{asset('template/css/playfair.css')}}">
<!-- Google fonts - Poppins-->
<link rel="stylesheet" href="{{asset('template/css/popkins.css')}}">
<!-- swiper-->
<link rel="stylesheet" href="{{asset('template/css/swiper.min.css')}}">
<!-- Magnigic Popup-->
<link rel="stylesheet" href="{{asset('template/css/magnific-popup.css')}}">
<!-- theme stylesheet-->
<link rel="stylesheet" href="{{asset('template/css/style.css')}}">
<!-- Custom stylesheet - for your changes-->
<link rel="stylesheet" href="{{asset('template/css/custom.css')}}">
<link rel="stylesheet" href="{{asset('template/css/fontawesome.min.css')}}">
<link rel="stylesheet" href="{{asset('template/css/all.min.css')}}">
<!-- Favicon-->
<link rel="shortcut icon" href="{{asset('/img/dinner.png')}}">
<!-- Font Awesome CSS-->
<link rel="stylesheet" href="{{asset('template/css/all.css')}}">
