@if(Session::has('message'))
    <div class="alert alert-success alert-dismissible" role="alert" id="myAlert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        <strong><i class="fa fa-check-circle"></i> Success !</strong>  {{ Session::get('message') }}
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger alert-dismissible" role="alert" id="myAlert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        <strong><i class="fa fa-check-circle"></i> Error!</strong>  {{ Session::get('error') }}
    </div>
@endif
