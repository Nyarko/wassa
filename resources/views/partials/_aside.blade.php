<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
<!-- Brand Logo -->
<router-link to="/dashboard" class="brand-link">
  <img src="./img/cups.png" alt="App Logo" class="brand-image img-circle elevation-3"
       style="opacity:.8">
  <span class="brand-text font-weight-light">InkCodeKids</span>
</router-link>

<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user panel (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
      <img src="{{Auth::user()->photo}}" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
      <a href="#" class="d-block">{{Auth::user()->name}}</a>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
       <li class="nav-item">
         <router-link :to="{name: 'dashboard'}" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt text-primary"></i>
            <p>Dashboard</p>
         </router-link>
       </li>

      <li class="nav-item">
        <router-link :to="{name: 'users'}" class="nav-link">
          <i class="nav-icon fas fa-user text-danger"></i>
          <p>Users</p>
        </router-link>
      </li>

      <li class="nav-item">
        <router-link :to="{name: 'project'}" class="nav-link">
          <i class="nav-icon fas fa-star text-info"></i>
          <p>Projects</p>
        </router-link>
      </li>

      <li class="nav-item">
        <router-link :to="{name: 'message'}" class="nav-link">
          <i class="nav-icon fas fa-envelope"></i>
          <p>Messages</p>
        </router-link>
      </li>

      <li class="nav-item">
        <router-link :to="{ name: 'carousel'}" class="nav-link">
          <i class="nav-icon fas fa-snowboarding" style="color:deeppink"></i>
          <p>Carousel</p>
        </router-link>
      </li>

      <li class="nav-item">
        <router-link :to="{name: 'gallery'}" class="nav-link">
          <i class="nav-icon fas fa-image" style="color:green"></i>
          <p>Gallery</p>
        </router-link>
      </li>

      <li class="nav-item">
        <router-link :to="{name: 'comment'}" class="nav-link">
          <i class="nav-icon fas fa-comment-alt" style="color:ghostwhite"></i>
          <p>Comments</p>
        </router-link>
      </li>

      <li class="nav-item">
        <a href="{{route('logout')}}" class="nav-link">
          <i class="nav-icon fas fa-sign-out-alt text-warning"></i>
          <p>Logout</p>
        </a>
      </li>

    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
