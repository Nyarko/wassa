<!-- jQuery-->
<script type="application/javascript" src="{{asset('template/js/jquery.min.js') }}"></script>
<!-- Bootstrap JS bundle - Bootstrap + PopperJS-->
<script type="application/javascript" src="{{asset('template/js/bootstrap.bundle.min.js') }}"></script>
<!-- Magnific Popup - Lightbox for the gallery-->
<script type="application/javascript" src="{{asset('template/js/jquery.magnific-popup.min.js') }}"></script>
<!-- Smooth scroll-->
<script type="application/javascript" src="{{asset('template/js/smooth-scroll.polyfills.min.js') }}"></script>
<!-- Bootstrap Select-->
<script type="application/javascript" src="{{asset('template/js/bootstrap-select.min.js') }}"></script>
<!-- Object Fit Images - Fallback for browsers that don't support object-fit-->
<script type="application/javascript" src="{{asset('template/js/ofi.min.js') }}"></script>
<!-- Swiper Carousel                       -->
<script type="application/javascript" src="{{asset('template/js/swiper.min.js') }}"></script>
{{-- <script>var basePath = ''</script> --}}
<!-- Main Theme JS file    -->
<script type="application/javascript" src="{{asset('template/js/theme.js') }}"></script>

<script type="application/javascript" src="{{asset('template/js/all.min.js') }}"></script>

<script type="application/javascript" src="{{asset('template/js/fontawesome.min.js') }}"></script>

<script type="application/javascript" src="{{asset('js/custom.js') }}"></script>
