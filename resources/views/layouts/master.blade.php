<!DOCTYPE html>
<html lang="en">
<head>
  @include('partials._head')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

   @include('partials._nav')
   @include('partials._aside')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <router-view></router-view>
          <!-- set progressbar -->
          <vue-progress-bar></vue-progress-bar>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('partials._footer')
</div>
<!-- ./wrapper -->
<script src="/js/app.js"></script>
</body>
</html>
