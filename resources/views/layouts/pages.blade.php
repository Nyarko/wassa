<!DOCTYPE html>
<html lang="en">
  <head>
   @include('partials._pagestyle')
  </head>
  <body>
   <div class="wrapper" style="padding-top: 72px;">
     @include('pages.header')

       @include('partials.loader')
       <div class="container-fluid">
           @yield('section')
       </div>

    @include('pages.footer')

   </div>
   {{--<script src="/js/app.js" type="application/javascript"></script>--}}
   @include('partials._pagescript')
  </body>

</html>
